﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartialClasesExample
{
    public partial class PartialCustomer
    {
        public override string ToString()
        {
            return this.firstName + " " + this.lastName;
        }
    }
}