﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PartialMethods
{
    public partial class SamplePartialClass
    {
        // Partial Methods can be declared only within a Partial Classes
        partial void SamplePartialMethod();  // for partial methods:  one part must specify only method definition (only signature)

        partial void SamplePartialMethod()    // other part must have the implementation
        {
            Console.WriteLine("SamplePartialMethod Invoked");
        }

        public void PublicMethod()
        {
            Console.WriteLine("Public Method Invoked");
            SamplePartialMethod();
        }


    }
}
