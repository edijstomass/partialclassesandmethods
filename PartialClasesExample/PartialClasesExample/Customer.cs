﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartialClasesExample
{
    public class Customer
    {
        private string firstName;
        private string lastName;

        public string FirstName
        {
          get
            {
                return this.firstName;
            }
            set
            {
               this.firstName = value;
            }
        }

        public string LastName { get => lastName; set => lastName = value; }


        public override string ToString()
        {
            return this.firstName + " " + this.lastName;
        }

    }
}