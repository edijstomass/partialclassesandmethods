﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PartialClasesExample
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region"Lesson One"
            Customer C1 = new Customer();
            C1.FirstName = "Edijs";
            C1.LastName = "Tomass";
            string fullName1 = C1.ToString();
            Response.Write("Fullname = " + fullName1 + "<br/>");

            PartialCustomer C2 = new PartialCustomer();
            C2.FirstName = "Edijs";
            C2.LastName = "Tomass";
            string fullName2 = C2.ToString();
            Response.Write("Fullname = " + fullName2 + "<br/>");

            TextBox1.Text = "Hello World!";
            #endregion

            SamplePartialClass obj = new SamplePartialClass();
            obj.GetData();
            obj.CustomerMethod();
            obj.EmployeeMethod();

        }
    }



    public class Sample
    {
        public void GetData()
        {

        }
    }

    interface IEmployee
    {
        void EmployeeMethod();
    }

    interface ICustomer
    {
        void CustomerMethod();
    }

}