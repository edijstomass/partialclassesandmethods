﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartialClasesExample
{
    public partial class PartialCustomer
    {
        private string firstName;
        private string lastName;

        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
            }
        }

        public string LastName { get => lastName; set => lastName = value; }
    }
}